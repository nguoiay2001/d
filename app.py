from fastapi import FastAPI
from src.core.middleware import LoggingMiddleware
from src.api.base import router_auth
from src.api.v1 import router_point, router_pet, router_article, router_user, router_gpt
from fastapi.middleware.cors import CORSMiddleware
import uvicorn


app = FastAPI(
    title="FastAPI templates",
    version="0.1.0",
    description="This is template for example project FastAPI",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.middleware("http")(LoggingMiddleware())
app.include_router(router=router_auth.router)
app.include_router(router=router_point.router)
app.include_router(router=router_pet.router)
app.include_router(router=router_article.router)
app.include_router(router=router_user.router)
app.include_router(router=router_gpt.router)


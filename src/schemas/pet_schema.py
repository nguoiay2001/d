from pydantic import BaseModel


class PetSchema(BaseModel):
    pet_id: int
    name: str | None = None
    point_exchange: int | None = None

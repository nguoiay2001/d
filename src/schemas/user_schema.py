from pydantic import BaseModel
from src.schemas.point_chema import PointSchema


class UserSchema(BaseModel):
    email: str
    points: list[PointSchema] = []

    class Config:
        orm_mode = True


class UserInput(BaseModel):
    email: str
    username: str | None = None
    password: str
    birth_day: str | None = None
    gender: str | None = None
    usertype: int | None = None


class UserEdit(BaseModel):
    email: str | None = None
    username: str | None = None
    fullname: str | None = None
    password: str | None = None
    birth_day: str | None = None
    gender: str | None = None
    usertype: int | None = None


class UserLogin(BaseModel):
    email: str
    password: str
    usertype: int | None


class UserLevel(BaseModel):
    email: str
    mission: list[PointSchema] = []

from pydantic import BaseModel


class GPTSchema(BaseModel):
    id: int
    name: str

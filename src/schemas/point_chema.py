from pydantic import BaseModel
from datetime import datetime

class PointSchema(BaseModel):
    point: int
    user_id: int | None
    point_type: int
    
    class Config:
        orm_mode = True


class PointInput(BaseModel):
    start_day: str


class ExPointIn(BaseModel):
    exchage_type: int

class ExPointSchema(BaseModel):
    exchage_type: int
    pay: int | None
    get: int | None
    description: str | None


    
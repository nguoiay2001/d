from fastapi import Depends
from src.db.base import DBContext
from src.schemas.point_chema import PointSchema
from src.schemas.response_schema import ResponseModel
from src.models.models import Points, Users, ExchageRate, Mission
from src.db.base import SessionLocal
import datetime


async def create_point(point: PointSchema, user_id, db):
    point_model = Points()
    point_query = db.query(Points).filter(Points.user_id == user_id).first()

    point_model.point = point.point
    point_model.point_type = point.point_type
    point_model.user_id = user_id
    # #point_model.exchange_type = point.exchange_type

    # exchange_type = [2, 3]
    # if point.exchange_type in exchange_type:
    #     if (point.point + point_query.user.total_point) <= 0:
    #         return ResponseModel(
    #             status_code=False,
    #             message="Not enough point to exchange",
    #         )
    #     else:
    #         point_model.exchange_point = round(point.point/2)
    db.add(point_model)
    db.commit()
    db.close()

    user = db.query(Users).filter(Users.id == user_id).first()
    if user.total_point is None:
        user.total_point = 0
    user.total_point += point.point
    db.commit()
    db.refresh(user)
    return user


async def get_all_user_point(user_id, start_day, db):
    user = db.query(Users).filter(Users.id == user_id).first()
    list_point = []
    for point in user.points:
        if point.created_at.month == start_day.month:
            list_point.append(point)
    return list_point


async def user_give_point(current_user, point_for_recevier, db):
    user_receiver_type = (
        db.query(Users.usertype).filter(Users.id == point_for_recevier.user_id).first()
    )
    if user_receiver_type == 1:
        return {"Message": "You can not give point to teacher"}
    user_point = (
        db.query(Points).filter(Points.id == point_for_recevier.user_id).first()
    )
    user_point.point = user_point.point + point_for_recevier.point
    db.commit()
    db.refresh(user_point)

    return {
        "user_give": current_user,
        "data": user_point,
    }


async def get_point_from_mission(current_user_id, id_mission, db):
    mission = db.query(Mission).filter(Mission.id == id_mission).first()
    return 1
    # if mission.status == 2:
    #     return {"message": "Sorry This mission is not completed "}
    # point_user = db.query(Points).filter(Points.user_id == current_user_id)
    # point_user.point = point_user.point + mission.point
    # mission.status = 2
    # db.commit()
    # db.refresh(point_user)
    # db.refresh(mission)
    # return {"data": {point_user, mission}}


async def get_exchange_rate(expoint, db):
    data = (
        db.query(ExchageRate)
        .filter(ExchageRate.exchage_type == expoint.exchage_type)
        .first()
    )
    expoint.exchage_type = data.exchage_type
    expoint.pay = data.pay
    expoint.get = data.get
    expoint.description = data.description
    return expoint

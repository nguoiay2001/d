from src.models.models import Users
from src.services.auth_services import *
from src.schemas.response_schema import ResponseModel


# Check email existing in wing or lucete (lucete :2, wing: 3)
async def change_user_password(data, db):
    user_type = [2, 3]
    user_query = db.query(Users).filter(Users.email == data.email).first()
    if user_query and data.usertype in user_type:
        if user_query.email == data.email:
            if user_query.password_hash != data.password:
                user_query.password_hash = data.password
                db.commit()
                db.refresh(user_query)


async def check_user_existence(data, db):
    user = db.query(Users).filter(Users.email == data.email).first()
    return user


async def create_user(data, db):
    user_model = Users()
    user_model.email = data.email
    user_model.usertype = data.usertype
    user_model.password_hash = data.password
    db.add(user_model)
    db.commit()
    return {"message": "SUCCESS"}


async def edit_user(current_user, data, db):
    user = db.query(Users).filter(Users.id == current_user.id).first()
    format_string = "%Y-%m-%d %H:%M:%S"
    if data.birth_day is not None:
        birth_day = datetime.strptime(data.birth_day, format_string)
        user.birth_day = birth_day
    if data.fullname is not None:
        user.fullname = data.fullname
    if data.username is not None:
        user.username = data.username
    if data.password is not None:
        user.password_hash = data.password
    if data.gender is not None:
        user.gender = data.gender

    db.commit()
    db.refresh(user)
    return user


async def get_mission(user):
    pass

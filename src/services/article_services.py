from sqlalchemy.orm import Session
from src.models.models import Level
from typing import Annotated
from src.schemas.user_schema import UserSchema
from src.services.auth_services import *
from src.models.models import Users, Level
from src.db.base import DBContext
from src.db.base import SessionLocal


def get_db():
    with DBContext() as db:
        yield db


async def get_level(db):
    level = db.query(Level).all()
    return level


async def get_user_mission(current_user_id, db):
    user = db.query(Users).filter(Users.id == current_user_id).first()
    user_level = user.level_id
    level = db.query(Level).filter(Level.id == user_level).first()
    missions = level.missions
    return missions

import os
# from dotenv import load_dotenv
from sqlalchemy import create_engine,text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import  sessionmaker,relationship


# load_dotenv('.env')

# Digital Ocene database
DATABASE_URL = "postgresql://doadmin:AVNS_S_hy8ahCgNOKAuyZX--@luceteplus-do-user-13713274-0.b.db.ondigitalocean.com:25060/defaultdb?sslmode=require"


engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False,autoflush=False,bind=engine)
Base = declarative_base()


class DBContext:
    def __init__(self) -> None:
        self.db = SessionLocal()

    def __enter__(self):
        return self.db
    
    def __exit__(self, et, ev, traceback):
        self.db.close()
def raw_query(raw_query,kwargs):
    try:
        with engine.connect() as connection:
            query = text(raw_query)
            # Execute the raw SQL query, passing in any required parameters
            result = connection.execute(query,kwargs)
            # Fetch all rows from the executed query
            rows = result.fetchall()
            return rows
    except Exception as e:
        print(e)
    return []
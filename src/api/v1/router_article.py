from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
import src.services.article_services as ArticleService
from src.schemas.response_schema import ResponseModel
from src.db.base import DBContext
from typing import Annotated
from src.schemas.user_schema import UserSchema
from src.services.auth_services import *


def get_db():
    with DBContext() as db:
        yield db


router = APIRouter(
    prefix="/api/v1/level",
    tags=["Level"],
    dependencies=[]
)


@router.get("/get_level")
async def get_level(db: Session = Depends(get_db)):
   
    reponse = await ArticleService.get_level(db)

    return ResponseModel(
            status_code=True,
            message="get all level",
            data=reponse
        )

           
@router.get("/mission")
async def get_user_mission(current_user: Annotated[UserSchema, Depends(get_current_active_user)],
                           db: Session = Depends(get_db)
                        ):
    current_user_id = current_user.id
    reponse = await ArticleService.get_user_mission(current_user_id, db)
    return ResponseModel(
            status_code=True,
            message="get all mission of level",
            data=reponse
        )


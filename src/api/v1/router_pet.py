from fastapi import APIRouter, Depends
from src.db.base import DBContext, SessionLocal
from src.schemas.pet_schema import PetSchema
from src.schemas.response_schema import ResponseModel 
from sqlalchemy.orm import Session
import src.services.pet_services as PetService
from src.models.models import Pet


def get_db():
    with DBContext() as db:
        yield db


router = APIRouter(
    prefix="/api/v1/pet",
    tags=["Pet"],
    dependencies=[]
)


@router.get("/get_all_pet")
async def get_all_pet(db: Session = Depends(get_db)):
   
    reponse = await PetService.get_all_pet(db)

    return ResponseModel(
            status_code=True,
            message="get all pet",
            data=reponse
        )


@router.post("/get_pet_by_id")
async def get_all_pet(pet: PetSchema, db: Session = Depends(get_db)):
    pet = db.query(Pet).filter(Pet.id == pet.pet_id).first()
    return ResponseModel(
            status_code=True,
            message="get pet by id",
            data=pet
        )
import os
import openai
from fastapi import APIRouter
from pydantic import BaseModel
from typing import List
from fastapi import UploadFile, HTTPException


class Promt(BaseModel):
    message: str


router = APIRouter(prefix="/api/v1/gpt", tags=["GPT"], dependencies=[])

openai.api_key = "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"


@router.post("/chatbot")
def get_completion_from_messages(promt: Promt):
    messages = [
        {"role": "system", "content": "reply like a kid"},
        {"role": "user", "content": promt.message},
    ]

    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=messages,
        temperature=1,
    )

    res = response.choices[0].message["content"]
    return res


@router.get("/whisper")
def get_audio_from_message():
    audio_file = open(os.getcwd() + "/src/api/v1/static/sound/test1.mp3", "rb")
    transcript = openai.Audio.transcribe("whisper-1", audio_file)

    messages = [
        {"role": "system", "content": "reply like a kid"},
        {"role": "user", "content": transcript["text"]},
    ]

    print(transcript["text"])

    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=messages,
        temperature=1,
    )

    res = response.choices[0].message["content"]
    return res

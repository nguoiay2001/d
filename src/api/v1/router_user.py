from fastapi import APIRouter, Depends, HTTPException
from src.db.base import DBContext
from src.schemas.user_schema import UserSchema, UserInput, UserLogin, UserEdit
from typing import List, Annotated
from src.schemas.response_schema import ResponseModel
from src.services.user_services import *
from sqlalchemy.orm import Session
from src.models.models import Users


def get_db():
    with DBContext() as db:
        yield db


router = APIRouter(prefix="/api/v1/user", tags=["User"], dependencies=[])


@router.get("/me")
async def read_users_me(
    current_user: Annotated[UserSchema, Depends(get_current_active_user)]
):
    return ResponseModel(
        status_code=True, message="Return current user login", data=current_user
    )


@router.post("/create")
async def create_user_app(user_input: UserInput, db: Session = Depends(get_db)):
    if await check_user_existence(user_input, db):
        return ResponseModel(status_code=False, message="User exist")

    create_user(user_input, db)

    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user_input.email}, expires_delta=access_token_expires
    )

    return ResponseModel(
        status_code=True,
        message="Create User Success",
        data={"access_token": access_token, "token_type": "bearer"},
    )


@router.post("/edit_user")
async def update_user_info(
    current_user: Annotated[UserSchema, Depends(get_current_active_user)],
    data: UserEdit,
    db: Session = Depends(get_db),
):
    respone = await edit_user(current_user, data, db)

    return ResponseModel(status_code=True, message="Edit User", data=respone)

from src.db.base import DBContext, SessionLocal
from fastapi import APIRouter, Depends
from src.schemas.point_chema import PointSchema, PointInput, ExPointSchema
from src.schemas.article_schema import MissionSchema
from src.schemas.response_schema import ResponseModel
from src.models.models import Question
from sqlalchemy.orm import Session
import src.services.point_services as PointService
import src.services.pet_services as PetService
from src.services.auth_services import *
from datetime import datetime


def get_db():
    with DBContext() as db:
        yield db


router = APIRouter(prefix="/api/v1/point", tags=["Point"], dependencies=[])








@router.post("/create_point")
async def create_point(
    point: PointSchema,
    current_user: Annotated[UserSchema, Depends(get_current_active_user)],
    db: Session = Depends(get_db),
):
    reponse = await PointService.create_point(point, current_user.id, db)

    return ResponseModel(status_code=True, message="create points", data=reponse)






# @router.post("/get_all_user_point")
# async def get_all_user_point(
#     point: PointInput,
#     current_user: Annotated[UserSchema, Depends(get_current_active_user)],
# ):
#     db = SessionLocal()

#     format_string = "%Y-%m-%d %H:%M:%S"
#     start_day = datetime.strptime(point.start_day, format_string)
#     reponse = await PointService.get_all_user_point(current_user.id, start_day, db)

#     return ResponseModel(
#         status_code=True,
#         message="get user's list point",
#         data={
#             "email": current_user.email,
#             "total_point": current_user.total_point,
#             "list_point": reponse,
#         },
#     )

@router.get('/')
async def get_all_pet(
    current_user: Annotated[UserSchema,Depends(get_current_active_user)]
): 
    respone =  await PetService.get_pet_in_point(current_user)
    
    return respone


@router.post("/give_point")
async def user_give_point(
    point_for_recevier: PointSchema,
    current_user: Annotated[UserSchema, Depends(get_current_active_user)],
):
    db = SessionLocal()
    respone = await PointService.user_give_point(
        current_user, point_for_recevier, db
    )

    return ResponseModel(status_code=True, message="Sucess give point", data=respone)


# @router.post("/get_point_mission")
# async def get_point_mission(
#     id_mission: MissionSchema,
#     current_user: Annotated[UserSchema, Depends(get_current_active_user)],
# ):
#     db = SessionLocal()
#     respone = await PointService.get_point_from_mission(current_user.id, id_mission, db)
#     return respone


@router.post("/exchange_rate")
async def get_exchange_rate(expoint: ExPointSchema, db: Session = Depends(get_db)):
    reponse = await PointService.get_exchange_rate(expoint, db)
    return ResponseModel(status_code=True, message="Get Exchange Rate", data=reponse)


@router.get("/test")
async def get_question(db: Session = Depends(get_db)):
    data = db.query(Question).filter(Question.id == 1).first()
    return data

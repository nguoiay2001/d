from fastapi import APIRouter, Depends
from src.schemas.response_schema import ResponseModel
from src.schemas.user_schema import UserSchema, UserInput, UserLogin
from src.services.auth_services import *
from src.services.user_services import *
from src.db.base import DBContext
from sqlalchemy.orm import Session


def get_db():
    with DBContext() as db:
        yield db


router = APIRouter(
    prefix="/api/v1/auth",
    tags=["Authentication"],
    dependencies=[]
)

@router.post("/token", response_model=ResponseModel)
async def login_for_access_token(form_data: UserLogin, db: Session = Depends(get_db)):

    #Check if existance user on wing or lucete, 
    # then change the passsword
    await change_user_password(form_data, db)

    user = await authenticate_user(form_data.email, form_data.password)

    if not user:
        return ResponseModel(
            status_code=False,
            message="Incorrect username or password",
            data={"access_token": None, "token_type": "bearer"}
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
       data={"sub": user.email}, expires_delta=access_token_expires
    )
    
    return ResponseModel(
        status_code=True,
        message="Getting access token",
        data={"access_token": access_token, "token_type": "bearer"}
    )



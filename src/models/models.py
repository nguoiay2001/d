from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, JSON, Boolean
from src.db.base import Base
from sqlalchemy.orm import relationship
import datetime
from typing import List
from sqlalchemy.dialects.postgresql import ARRAY
import random
import string


class Users(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(30), default="username")
    password_hash = Column(String)
    usertype = Column(Integer)
    fullname = Column(String(50), default="fullname")
    birth_day = Column(DateTime)
    gender = Column(String, default="Boy")
    email = Column(String(50))
    total_point = Column(Integer, default=0)
    level_id = Column(Integer, default=1)
    avatar_url = Column(String)
    country_id = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    is_deleted = Column(Boolean)
    pet_ids = Column(Integer)
    points = relationship("Points", back_populates="user")

    def generate_username(self):
        self.fullname = "".join(random.choice(string.ascii_lowercase) for _ in range(8))

        return self.fullname


class Country(Base):
    __tablename__ = "country"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    country_code = Column(Integer)
    city = relationship("City", back_populates="country")


class City(Base):
    __tablename__ = "city"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    city_code = Column(Integer)
    country_id = Column(Integer, ForeignKey("country.id"))
    country = relationship("Country", back_populates="city")


class UserType(Base):
    __tablename__ = "usertype"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(30))


class Points(Base):
    __tablename__ = "points"
    id = Column(Integer, primary_key=True, autoincrement=True)
    point = Column(Integer)
    point_type = Column(Integer)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("Users", back_populates="points")


class PointType(Base):
    __tablename__ = "point_type"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)


class ExchageRate(Base):
    __tablename__ = "exchange_rate"
    id = Column(Integer, primary_key=True, autoincrement=True)
    exchage_type = Column(Integer)
    pay = Column(Integer)
    get = Column(Integer)
    description = Column(String)


class Pet(Base):
    __tablename__ = "pet"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    pet_info = Column(String)
    pet_type = Column(String)
    point_exchange = Column(Integer)
    url_model = Column(String)
    is_owner = Column(Boolean)
    is_special = Column(Integer)
    created_at = Column(DateTime)
    
    
class PetUser(Base): 
    __tablename__ = "user_pet"
    id = Column(Integer,primary_key=True,autoincrement=True)
    user_id = Column(Integer)
    pet_id = Column(Integer)
    created_at = Column(DateTime)


class Level(Base):
    __tablename__ = "level"
    id = Column(Integer, primary_key=True, autoincrement=True)
    level = Column(Integer)
    missions = relationship("Mission", back_populates="level")


class Mission(Base):
    __tablename__ = "mission"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    mission_type = Column(Integer)
    question_id = Column(Integer)
    level_id = Column(Integer, ForeignKey("level.id"))
    level = relationship("Level", back_populates="missions")
    point = Column(Integer)
    status = Column(Integer, default=1)


class Question(Base):
    __tablename__ = "question"
    id = Column(Integer, primary_key=True, autoincrement=True)
    content = Column(String)
    val_1 = Column(Boolean)
    val_2 = Column(Boolean)
    val_3 = Column(Boolean)
    val_4 = Column(Boolean)
